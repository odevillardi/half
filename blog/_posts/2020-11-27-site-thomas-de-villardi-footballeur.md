---
layout: post
title: "Site de Thomas de Villardi"
date: '2020-11-27T10:57:00+02:00'
category:
- projets
tags:
- web
- sport
img_url: /blog/posts-img/tdv-list.jpg
img_caption: "Capture d’écran du site de Thomas de Villardi"
active: blog
selection: oui
---
Nouveau petit projet sportif, la création d’un [site pour le footballeur professionnel Thomas de&nbsp;Villardi](https://thomas-de-villardi.com). Au programme, un [site statique](/solution/) monopage mettant l’accent sur les visuels, photos et vidéos.

## Objectif présence
Comme beaucoup de sportifs pros, Thomas de&nbsp;Villardi est présent sur les réseaux sociaux mais n’avait pas de **site dédié**. Nous avons donc travaillé ensemble pour combler ce manque et lui offrir un espace où il est maître de ce qu’il présente et de son [identité visuelle](/creation/).

![Captures du site Thomas de&nbsp;Villardi sur différents écrans](/blog/posts-img/tdv-post.jpg)

## Identité visuelle
Nous avons opté pour&nbsp;:
- un **style épuré** qui met en avant les photos de Thomas de&nbsp;Villardi en action et ses vidéos en plein écran&nbsp;;
- une police de caractère **dynamique et moderne**&nbsp;: Oswald&nbsp;;
- du noir, du blanc et une seule couleur pour les liens, le bleu de son club actuel [Austin Bold FC](https://austinboldfc.com/).

Afin d’obtenir un site **moderne**, **minimaliste** et à l’image de son sujet&nbsp;: **sobre**.

## Informations
Le site est également minimaliste dans son contenu&nbsp;:
- **liens vers les réseaux sociaux** et les clubs de Thomas de&nbsp;Villardi&nbsp;;
- **photos et vidéos** du footballeur en action&nbsp;;
- une **fiche profil**.

Pour en savoir plus, rendez-vous sur le [site de Thomas de&nbsp;Villardi](https://thomas-de-villardi.com)&nbsp;!
