---
layout: post
title: "Création de chambery-vegan.fr"
date: '2020-12-03T10:57:00+02:00'
category:
- projets
tags:
- web
- environnement
img_url: /blog/posts-img/chambery-vegan-list.jpg
img_caption: "Capture d’écran du site chambery-vegan.fr"
active: blog
selection: oui
---
L’offre végane prend de l’ampleur à Chambéry et à ses alentours. J’ai souhaité répertorier les “spots” (restaurants, bar, cafés…) ayant une offre végétale sur [un site collaboratif et attractif](https://chambery-vegan.fr).

![Capture d’écran du site chambery-vegan.fr](/blog/posts-img/chambery-vegan-list.jpg)

## Site statique et pseudo-dynamique
Voilà une petite nouveauté pour moi. Je reste fidèle à mon idée que les sites statiques peuvent être, très souvent, une excellente solution mais j’ai ajouté ici une subtilité. Le site est **généré chaque nuit pour mettre à jour les horaires** des spots.

Une fois de plus, c’est le choix logique étant donnée [l’offre végane actuelle à Chambéry](https://chambery-vegan.fr). Une petite douzaine d’établissements et une volonté de fournir uniquement les informations pratiques essentielles (le reste étant largement disponible et accessible sur leurs sites ou réseaux sociaux) rendent le site statique tout à fait adapté.

![Capture d’écran du site chambery-vegan.fr](/blog/posts-img/chambery-vegan-tous.jpg)

## Collaboratif
Pour maintenir à jour cette liste, je fournis un formulaire et le dépôt GitLab est accessible pour les plus téméraires.

![Capture d’écran du site chambery-vegan.fr](/blog/posts-img/chambery-vegan-collaboratif.jpg)

En conclusion, un petit projet qui colle à mes valeurs.

Découvrez [Chambéry Vegan](https://chambery-vegan.fr)&nbsp;!
