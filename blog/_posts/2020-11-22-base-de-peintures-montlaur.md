---
layout: post
title: "Migration de la base Montlaur"
date: '2020-11-22T15:57:00+02:00'
category:
- projets
tags:
- web
- art
img_url: /blog/posts-img/montlaur-base-ide.jpg
img_caption: "Fichier Yaml de la base Montlaur"
active: blog
---
Après le [redesign du site Montlaur.net](/blog/2018/04/07/site-montlaur.html), le second projet Montlaur vient de se terminer. L’objet&nbsp;: migrer une ancienne base de données des peintures vers un système collaboratif et accessible en ligne.

## Choix des technos
La base de données des peintures de Guy de Montlaur est maintenue par ses deux fils mais elle était centralisée sur un ordinateur dans une base Access.

L’objectif du projet était de rendre la base&nbsp;:
- **collaborative**, pour pouvoir être enrichie par plusieurs personnes&nbsp;;
- **visualisable en ligne**, pour pouvoir être consultée par d’autres membres de la famille.

Pour répondre à ces deux besoins, le choix s’est portée sur&nbsp;:
- un **projet privé GitLab**, qui permet la gestion des versions et la mise en ligne via GitLab Pages&nbsp;;
- un **site monopage statique** ([Jekyll](https://jekyllrb.com)), pour la visualisation de la base&nbsp;;
- une base dans un **fichier à plat en Yaml**, format simple, efficace et lisible.

La base étant, de fait, limitée en taille et modifiée assez peu souvent, ce choix était logique pour son efficacité et son apprentissage simple.

## Résultat
Dans Access, les fiches ressemblaient à ça&nbsp;:

![Fiche dans Access](/blog/posts-img/montlaur-base-access.jpg)

La simplicité d’affichage a été conservée, mais a été modernisée pour être adaptée au Web et particulièrement au mobile&nbsp;:

![Nouvelle fiche](/blog/posts-img/montlaur-base-fiche.png)

Et une vue de la liste&nbsp;:

![Liste des peintures](/blog/posts-img/montlaur-base-liste.png)

Objectif rempli&nbsp;: un résultats simple et efficace, des clients satisfaits&nbsp;!
