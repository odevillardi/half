---
layout: post
title: "Redesign de Montlaur.net"
date: '2018-04-07T14:47:00+02:00'
category:
- projets
tags:
- web
- art
img_url: /blog/posts-img/montlaur-list.jpg
active: blog
---
Afin de moderniser le site présentant <a href="https://montlaur.net/">l’œuvre de Guy de Montlaur</a> et pour y intégrer un blog précédemment sur Wordpress, j’ai créé le nouveau <a href="https://montlaur.net/">montlaur.net</a> en statique avec le moteur Jekyll.

## Mettre en avant les peintures
<img src="/blog/posts-img/montlaur-home.png" alt="Capture d'écran du site Web montlaur.net">

L’objectif premier de ce redesign était de mettre au maximum en valeur les œuvres de Guy de Montlaur. Place donc à un design très épuré sur des fonds noirs, blancs ou gris afin de laisser place nette aux peintures.

<img src="/blog/posts-img/montlaur-peintures.png" alt="Capture d'écran du site Web montlaur.net">

Le petit challenge sur ce site a été de faire cette "lightbox" en CSS uniquement. J’ai utilisé pour l'occasion les unités vw et vh, et je me suis un peu amusé avec les filtres.

<img src="/blog/posts-img/montlaur-peinture.png" alt="Capture d'écran du site Web montlaur.net">

Pour le reste, du classique et quelques originalités. J’ai aimé utiliser le principe des colonnes pour la biographie et j’ai passé pas mal de temps à optimiser le menu pour chaque point de rupture avec un résultat fort plaisant.

Le multilingue a été implémenté pour faire suite aux expositions aux États-Unis et en Russie.

<p class="text-center not-lead navigation">
  <a class="button button-primary" href="https://montlaur.net">Voir le site de Guy de Montlaur</a>
</p>
