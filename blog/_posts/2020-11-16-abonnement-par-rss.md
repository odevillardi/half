---
layout: post
title: "S’abonner par RSS"
date: '2020-11-16T15:57:00+02:00'
category:
- divers
tags:
- web
- low-tech
img_url: /blog/posts-img/rss.jpg
img_caption: "Logo RSS"
active: blog
---
Qu’est-ce que le RSS&nbsp;? Comment ça marche&nbsp;? Et pourquoi pas une bonne vieille newsletter comme tout le monde&nbsp;?

## C’est quoi RSS
[RSS](https://fr.wikipedia.org/wiki/RSS) (Really Simple Syndication) est “*une forme de souscription dans laquelle une partie d'un site est accessible depuis d'autres sites*” ([Wikipedia](https://fr.wikipedia.org/wiki/Syndication_de_contenu)) ou autrement dit&nbsp;: **c’est un moyen très simple de s’abonner à un site**.

La plupart des blogs et des sites d’actualité proposent des flux RSS aux visiteurs.

## Pourquoi RSS
Pour plusieurs très bonnes raisons&nbsp;:
- c’est un [**format ouvert**](https://fr.wikipedia.org/wiki/Format_ouvert)&nbsp;;
- c’est non intrusif&nbsp;;
- c’est une **technologie sobre**&nbsp;;
- ça ne demande **aucune donnée personnelle** à son utilisateur&nbsp;;
- ça a été créé par [**Aaron Swartz**](https://fr.wikipedia.org/wiki/Aaron_Swartz).

Eh oui, comme vous dîtes, ça a l’air tout bonnement formidable&nbsp;!

Ça l’est.

## Pourquoi c’est mieux qu’une Newsletter
C’est simple&nbsp;:
- parce que vous n’avez à fournir aucune donnée personnelle pour vous abonner&nbsp;;
- parce que vous choisissez le moment / la fréquence à laquelle vous souhaitez lire le contenu&nbsp;;
- parce que vous n’avez aucun risque que votre email soit utilisé à mauvais escient (puisque vous ne donnez pas votre email&nbsp;!).

Et pour bien d’autres raisons que vous comprendrez dès que vous vous y serez mis :)

## Ok, j’ai compris, je veux m’abonner par RSS
Formidable&nbsp;!

Pour ce faire&nbsp;:
- vous devez choisir un aggrégateur RSS&nbsp;:
  - dans votre navigateur en faisant une recherche sur votre moteur préféré&nbsp;;
  - sur votre téléphone en faisant une recherche sur le store de votre téléphone&nbsp;;
- puis vous ajoutez les RSS de vos sites préférés&nbsp;;
- c’est fini.

Voici le lien RSS de Half revolution&nbsp;: [https://half.re/feed.xml](https://half.re/feed.xml)

Bonne lecture.
