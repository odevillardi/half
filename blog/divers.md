---
title: "Divers"
description: "Tous les billets de blog qui ne rentrent pas dans des cases."
layout: post-list
active: blog
category: divers
---
## Des billets divers
Et variés.

Parce que, parfois, on ne sait pas où les ranger.
