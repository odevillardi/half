---
title: "Blog"
description: "Tous les billets de blog, des liens, des projets et des retours d’expérience."
layout: post-list
active: blog
category: blog
---
## Tous les billets
Des retours d’expériences, des projets revus en détail et des liens vers des choses intéressantes.

**Bienvenue sur le blog de Half revolution**.

Oh, et [abonnez-vous par RSS](https://half.re/feed.xml) <small>([c’est quoi&nbsp;?](https://half.re/blog/2020/11/16/abonnement-par-rss.html))</small>, c’est quand même plus moderne et moins intrusif qu’une Newsletter.
